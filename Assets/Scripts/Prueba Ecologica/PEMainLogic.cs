﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;


public class PEMainLogic : MonoBehaviour
{
	//Script variables
	MetricsProcessing metScript;
	GameSaveLoad loader;
	ProgressHandler saveHandler;
	public FadeInOut fadeScript;
	public IntroGamesConfiguration configuration;
	PackLogic packScript;
	AirportRouteLogic routeLogic;
	FlyPlaneLogic flyLogic;
	WaitingLogic waitLogic;
	PickUpCoinsLogic coinsLogic;
	UnPackLogic unPackLog;
	FirstObj firstunPackLog;


	//logic variables
	public string miniGame;
	public bool curGameFinished = false;
	public string nextGame;
	public float timeOfFade;
	public int minAge1 = 4;
	public int maxAge1 = 6;
	public int minAge2 = 7;
	public int maxAge2 = 9;
	public int minAge3 = 10;
	public int maxAge3 = 12;


	//info input vars


	//...........pack game vars
	public List<GameObject> tierUsed = new List<GameObject>();
	public List<GameObject> weatherTierUsed = new List<GameObject>();
	public GameObject[] sPoints;
	public int numObjPerRound;
	public string[] oTier1;
	public string[] oTier2;
	public string[] oTier3;
	public string[] oWTier1;
	public string[] oWTier2;
	public string[] oWTier3;

	//unpack vars
	public List<GameObject> unPObj = new List<GameObject>();
	
	
	//data variables
	public int ageOfPlayer;
	public string nameOfPlayer;
	public string addressOfPlayer;
	public string birthdayOfPlayer;
	public string currentDate;
	float testSectionTime;
	float totalTestTime;

	float lat;




	// Use this for initialization
	void Start ()
	{
		metScript = GameObject.Find("Metrics").GetComponent<MetricsProcessing>();
		fadeScript = Camera.main.GetComponent<FadeInOut>();
		packScript = GameObject.Find("Pack").GetComponent<PackLogic>();
		routeLogic = GameObject.Find("AirportRoute").GetComponent<AirportRouteLogic>();
		flyLogic = GameObject.Find("FlyPlane").GetComponent<FlyPlaneLogic>();
		waitLogic = GameObject.Find("WaitingRoom").GetComponent<WaitingLogic>();
		coinsLogic = GameObject.Find("PickUpCoins").GetComponent<PickUpCoinsLogic>();
		unPackLog = GameObject.Find("UnPack").GetComponent<UnPackLogic>();
        saveHandler = GetComponent<ProgressHandler>();	
		sPoints = GameObject.FindGameObjectsWithTag("SpawnPoint");

		loader = GetComponent<GameSaveLoad>();
		loader.Load(GameSaveLoad.game.introGames);
		configuration = (IntroGamesConfiguration)loader.configuration;

		Application.targetFrameRate = 60;
		miniGame = "FadeOut";
		nextGame = "InputAge";//nextGame = "InputAge";
		
		ageOfPlayer = 0;
		nameOfPlayer = "0";
		addressOfPlayer = "0";
		birthdayOfPlayer = "0";
		currentDate = "0";
		SetLevel();
		Camera.main.orthographic = true;
		
	}
	
	// Update is called once per frame
	void Update ()
	{

		totalTestTime += Time.deltaTime;

		switch(miniGame){
		case "InputAge":

			testSectionTime += Time.deltaTime;
			if(curGameFinished)
			{

				nextGame = "BuyTicket";	
				curGameFinished = false;
				SaveLevelProgress();
				miniGame = "BuyTicket";

			}
			break;	
		case "BuyTicket":
			testSectionTime += Time.deltaTime;
			if(curGameFinished)
			{
				nextGame = "Pack";
				curGameFinished = false;
				SaveLevelProgress();
				miniGame = "FadeOut";
				packScript.gameState = "Intro";
			}
			break;
		case "Pack":
			testSectionTime += Time.deltaTime;
			if(curGameFinished)
			{
				nextGame = "PlanRoute";
				curGameFinished = false;
				SaveLevelProgress();
				miniGame = "FadeOut";
			}
			else
			{
				
			}
			break;
		case "PlanRoute":
			testSectionTime += Time.deltaTime;
			if(curGameFinished)
			{
				nextGame = "WaitingRoom";
				curGameFinished = false;
				SaveLevelProgress();
				miniGame = "FadeOut";
			}
			else
			{
				if(routeLogic.state == "")
				{
					routeLogic.state = "Intro";
				}
			}
			break;
		case "WaitingRoom":
			testSectionTime += Time.deltaTime;
			if(curGameFinished)
			{
				nextGame = "FlyPlane";
				curGameFinished = false;
				SaveLevelProgress();
				miniGame = "FadeOut";
			}
			break;
		case "FlyPlane":
			testSectionTime += Time.deltaTime;
			if(curGameFinished)
			{
				nextGame = "PickUpCoins";
				curGameFinished = false;
				SaveLevelProgress();
				miniGame = "FadeOut";
			}
			break;
		case "PickUpCoins":
			testSectionTime += Time.deltaTime;
			if(curGameFinished)
			{
				nextGame = "UnPack";
				curGameFinished = false;
				SaveLevelProgress();
				miniGame = "FadeOut";
			}
			break;
		case "UnPack":
			testSectionTime += Time.deltaTime;
			if(curGameFinished)
			{
				nextGame = "End";
				curGameFinished = false;
				SaveLevelProgress();
				miniGame = "FadeOut";
			}
			else
			{
				
			}
			break;
		case "FadeIn":
			
			if(fadeScript.mat.color.a <= 0f)
			{
				
				miniGame = nextGame;
				break;
			}
			if(!fadeScript.fade && fadeScript.mat.color.a >= 1f)
			{
				fadeScript.FadeFunc(true, timeOfFade);
			}
			break;
		case "FadeOut":
			
			if(!fadeScript.fade && fadeScript.mat.color.a <= 0f)
			{
				fadeScript.FadeFunc(false, timeOfFade);
			}
			
			break;
		case "End":
            miniGame = "Default";
			metScript.Process();
			SaveProgress();
			saveHandler.PostProgress(false);
			break;
		case "Default":
            if(!saveHandler.saving)
            {
                Application.LoadLevel("Login");
            }
            break;
		}
	}
	void SaveLevelProgress()
	{
		//totalTestTime += testSectionTime; Se estaba duplicando el tiempo ya que en update tambien se actualiza la variable totalTestTime
		switch(miniGame){
		case "InputAge":
			saveHandler.addLevelData("playerAge", ageOfPlayer);
			saveHandler.addLevelData("playerBirth", birthdayOfPlayer);
			saveHandler.addLevelData("InputAgeTimeOfComp", (int)testSectionTime);
			testSectionTime = 0;
			saveHandler.setLevel();
			break;	
		case "BuyTicket":
			/*if(ageOfPlayer <= maxAge1)
			{
				saveHandler.addLevelData("playerName", nameOfPlayer);
			}
			else if(ageOfPlayer > maxAge1 && ageOfPlayer <= maxAge2)
			{
				saveHandler.addLevelData("playerName", nameOfPlayer);
				saveHandler.addLevelData("playerAddress", addressOfPlayer);
			}
			else if(ageOfPlayer > maxAge2)
			{
				saveHandler.addLevelData("playerName", nameOfPlayer);
				saveHandler.addLevelData("playerAddress", addressOfPlayer);
				saveHandler.addLevelData("currentDate", currentDate);
			}*/
			saveHandler.addLevelData("playerName", nameOfPlayer);
			saveHandler.addLevelData("playerAddress", addressOfPlayer);
			saveHandler.addLevelData("currentDate", currentDate);

			saveHandler.addLevelData("buyTicketTimeOfComp", (int)testSectionTime);
			testSectionTime = 0;
			saveHandler.setLevel();
			break;
		case "Pack":
			saveHandler.addLevelData("normalPackScore", packScript.normListCount-1);
			saveHandler.addLevelData("reversePackScore", packScript.reverseListCount-1);
			saveHandler.addLevelData("packTimeOfComp", (int)testSectionTime);
			saveHandler.addLevelData("packTypeOfWeather", packScript.weather);
			saveHandler.addLevelData("weatherObjectPacked", packScript.weatherObjPacked);

			saveHandler.addLevelData("objectToRemember", packScript.unPackObj);
			metScript.values[6] = packScript.reverseAElementList.Count;
			metScript.values[7] = packScript.normListCount;


		
			testSectionTime = 0;
			saveHandler.setLevel();
			break;
		case "PlanRoute":
			saveHandler.addLevelData ("timeOfLab1", routeLogic.times [0]);
			saveHandler.addLevelData ("timeOfLab2", routeLogic.times [1]);
			saveHandler.addLevelData ("timeOfLab3", routeLogic.times [2]);
			saveHandler.addLevelData ("latenciesOfLab1", routeLogic.latency [0]);
			saveHandler.addLevelData ("latenciesOfLab2", routeLogic.latency [1]);
			saveHandler.addLevelData ("latenciesOfLab3", routeLogic.latency [2]);
			saveHandler.addLevelData ("hitsOfLab1", routeLogic.hits [0]);
			saveHandler.addLevelData ("hitsOfLab2", routeLogic.hits [1]);
			saveHandler.addLevelData ("hitsOfLab3", routeLogic.hits [2]);
			int totalHitsLabs = routeLogic.hits [0] + routeLogic.hits [1] + routeLogic.hits [2];
			saveHandler.addLevelData ("totalHits", totalHitsLabs);
			saveHandler.addLevelData ("XHits", (float)totalHitsLabs / 3);
			saveHandler.addLevelData ("crossesOfLab1", routeLogic.crosses [0]);
			saveHandler.addLevelData ("crossesOfLab2", routeLogic.crosses [1]);
			saveHandler.addLevelData ("crossesOfLab3", routeLogic.crosses [2]);
			int totalCrossesLabs = routeLogic.crosses [0] + routeLogic.crosses [1] + routeLogic.crosses [2];
			saveHandler.addLevelData ("totalCrosses", totalCrossesLabs);
			saveHandler.addLevelData ("XCrosses", (float) totalCrossesLabs / 3);
			saveHandler.addLevelData ("deadEndsOfLab1", routeLogic.deadEnds [0]);
			saveHandler.addLevelData ("deadEndsOfLab2", routeLogic.deadEnds [1]);
			saveHandler.addLevelData ("deadEndsOfLab3", routeLogic.deadEnds [2]);
			int totalDeadEndsLabs = routeLogic.deadEnds [0] + routeLogic.deadEnds [1] + routeLogic.deadEnds [2];
			saveHandler.addLevelData("totalDeadEnds", totalDeadEndsLabs);
			saveHandler.addLevelData("XDeadEnds", (float) totalDeadEndsLabs/3);
			saveHandler.addLevelData("airportRouteTimeOfComp", (int)testSectionTime);

			metScript.values[3] = routeLogic.deadEnds[0] + routeLogic.deadEnds[1] + routeLogic.deadEnds[2];
			metScript.values[4] = routeLogic.crosses[0] + routeLogic.crosses[1] + routeLogic.crosses[2];
			metScript.values[5] = testSectionTime;
			testSectionTime = 0;
			saveHandler.setLevel();
			break;
		case "WaitingRoom":
			saveHandler.addLevelData("waitRoomCorrect", waitLogic.correct);
			saveHandler.addLevelData("waitRoomIncorrect", waitLogic.incorrect);
			saveHandler.addLevelData("waitRoomMissed", waitLogic.missed);
			saveHandler.addLevelData("timeBetweenFlights", waitLogic.timeBetweenCalls);
			metScript.values[9] = waitLogic.correct;

			testSectionTime = 0;
			saveHandler.setLevel();
			break;
		case "FlyPlane":
			saveHandler.addLevelData("flyPlaneCorrect", flyLogic.correct);
			saveHandler.addLevelData("flyPlaneIncorrect", flyLogic.incorrect);
			saveHandler.addLevelData("flyPlaneMissed", flyLogic.missed);
			saveHandler.addLevelData("flyPlaneCorrectGreen", flyLogic.correctGreen);
			saveHandler.addLevelData("flyPlaneIncorrectGreen", flyLogic.incorrectGreen);
			saveHandler.addLevelData("flyPlaneMissedGreen", flyLogic.missedGreen);
			saveHandler.addLevelData("flyPlaneTimeforInput", flyLogic.timeForInput);
			saveHandler.addLevelData("flyPlaneTimeOfComp", (int)testSectionTime);
			saveHandler.addLevelData("InputLatency", flyLogic.latency);
			int perc = flyLogic.correct * 100;
			perc = (int)perc / 15;

			metScript.values[1] = perc;

			for(int i = 0; i < flyLogic.latency.Length; i++)
			{
				lat += flyLogic.latency[i];
			}
			lat = lat / flyLogic.latency.Length;
//			metScript.values[2] = lat;
			testSectionTime = 0;
			saveHandler.setLevel();
			break;
		case "PickUpCoins":
			saveHandler.addLevelData("pUpCoinsMinCorrect", coinsLogic.minuteCorrect);
			saveHandler.addLevelData("pUpCoinsMinIncorrect", coinsLogic.minuteIncorrect);
			saveHandler.addLevelData("pUpCoinsMinMissed", coinsLogic.minuteMissed);
			saveHandler.addLevelData("pUpCoinsExtraCorrect", coinsLogic.extraCorrect);
			saveHandler.addLevelData("pUpCoinsExtraIncorrect", coinsLogic.extraIncorrect);
			saveHandler.addLevelData("pUpCoinsExtraMissed", coinsLogic.extraMissed);
			saveHandler.addLevelData("CoinsSelected", coinsLogic.coinsSelected);
			saveHandler.addLevelData("pUpCoinsTimeOfComp", (int)testSectionTime);
			saveHandler.addLevelData("clickBeforeMin", coinsLogic.beforeMinClick);
			metScript.values[0] = coinsLogic.minuteCorrect + coinsLogic.extraCorrect;

			testSectionTime = 0;
			saveHandler.setLevel();
			break;
		case "UnPack":

			saveHandler.addLevelData ("unPackedFirstObjs", unPackLog.unPackedFObjs);
			saveHandler.addLevelData ("unPackedFirstCorrect", unPackLog.correctFirstObj); //firstunPackLog.correct
			saveHandler.addLevelData ("unPackedFirstPerc", (float)(unPackLog.correctFirstObj/ 3) * 100); //(firstunPackLog.correct / 3 * 100)
			saveHandler.addLevelData ("unPackCorrectSample1", unPackLog.correct [0]);
			saveHandler.addLevelData ("unPackCorrectSample2", unPackLog.correct [1]);
			saveHandler.addLevelData ("unPackCorrectSample3", unPackLog.correct [2]);
			saveHandler.addLevelData ("unPackIncorrectSample1", unPackLog.incorrect [0]);
			saveHandler.addLevelData ("unPackIncorrectSample2", unPackLog.incorrect [1]);
			saveHandler.addLevelData ("unPackIncorrectSample3", unPackLog.incorrect [2]);
			saveHandler.addLevelData ("unPackRepeatedSample1", unPackLog.repeated [0]);
			saveHandler.addLevelData ("unPackRepeatedSample2", unPackLog.repeated [1]);
			saveHandler.addLevelData ("unPackRepeatedSample3", unPackLog.repeated [2]);
			saveHandler.addLevelData ("unPackFourFirstSample", unPackLog.fourFirst);
			saveHandler.addLevelData ("unPackFourLastSample", unPackLog.fourLast);
			saveHandler.addLevelData ("unPackGroupingSample", unPackLog.grouping);
			saveHandler.addLevelData ("unPackSpacialPrecisionSample", unPackLog.spacialPres);
			saveHandler.addLevelData ("unPackPicTime", (int)unPackLog.timeOfPic);
			saveHandler.addLevelData ("unPackTimeOfComp", (int)testSectionTime);
			int totalUnPackCorrect = unPackLog.correct [0] + unPackLog.correct [1] + unPackLog.correct [2];
			saveHandler.addLevelData("unPackTotalCorrect", totalUnPackCorrect);
			saveHandler.addLevelData("unPackXTotalCorrect", (float)totalUnPackCorrect/3);
			saveHandler.addLevelData("unPackPercTotalCorrect", (float)totalUnPackCorrect/(unPackLog.objSequence.Count*3)*100); //checar si esta bien este porciento
			int totalUnPackIncorrect = unPackLog.incorrect[0] + unPackLog.incorrect[1] + unPackLog.incorrect[2];
			saveHandler.addLevelData("unPackTotalIncorrect", totalUnPackIncorrect);
			saveHandler.addLevelData("unPackXTotalIncorrect", (float)totalUnPackIncorrect/3);
			saveHandler.addLevelData("unPackPercTotalIncorrect", (float)totalUnPackIncorrect/(unPackLog.objSequence.Count*3)*100); //checar si esta bien este porciento


			metScript.values[8] = unPackLog.correct[0] + unPackLog.correct[1] + unPackLog.correct[2] + unPackLog.correct[3];
			testSectionTime = 0;
			saveHandler.setLevel();
			break;
		}

	}
	void SaveProgress()
	{
		saveHandler.createSaveBlock("PruebaEcologica", (int)totalTestTime, 0, 0, 7);
		saveHandler.addLevelsToBlock();
		Debug.Log(saveHandler.ToString());
	}
	void SetLevel()
	{
		flyLogic.arrowColorTutorial = configuration.miniGame.flyPlane.arrowColorTutorial;
		flyLogic.arrowDirTutorial = configuration.miniGame.flyPlane.arrowDirectionTutorial;
        flyLogic.arrowColor = configuration.miniGame.flyPlane.arrowColor;
		flyLogic.arrowDir = configuration.miniGame.flyPlane.arrowDirection;

	}
}