﻿using UnityEngine;
using System;
using System.Collections;
using System.Collections.Generic;

public class AndroidEventListener : MonoBehaviour
{

	#if UNITY_ANDROID
	Login loginScript;
	void Start()
	{
		loginScript = GameObject.Find("Interface").GetComponent<Login>();
	}
	void OnEnable()
	{
		// Listen to all events for illustration purposes
		GoogleIABManager.billingSupportedEvent += billingSupportedEvent;
		GoogleIABManager.billingNotSupportedEvent += billingNotSupportedEvent;
		GoogleIABManager.queryInventorySucceededEvent += queryInventorySucceededEvent;
		GoogleIABManager.queryInventoryFailedEvent += queryInventoryFailedEvent;
		GoogleIABManager.purchaseCompleteAwaitingVerificationEvent += purchaseCompleteAwaitingVerificationEvent;
		GoogleIABManager.purchaseSucceededEvent += purchaseSucceededEvent;
		GoogleIABManager.purchaseFailedEvent += purchaseFailedEvent;
	}
	
	
	void OnDisable()
	{
		// Remove all event handlers
		GoogleIABManager.billingSupportedEvent -= billingSupportedEvent;
		GoogleIABManager.billingNotSupportedEvent -= billingNotSupportedEvent;
		GoogleIABManager.queryInventorySucceededEvent -= queryInventorySucceededEvent;
		GoogleIABManager.queryInventoryFailedEvent -= queryInventoryFailedEvent;
		GoogleIABManager.purchaseCompleteAwaitingVerificationEvent += purchaseCompleteAwaitingVerificationEvent;
		GoogleIABManager.purchaseSucceededEvent -= purchaseSucceededEvent;
		GoogleIABManager.purchaseFailedEvent -= purchaseFailedEvent;
	}
	
	
	
	void billingSupportedEvent()
	{
		Debug.Log( "billingSupportedEvent" );
	}
	
	
	void billingNotSupportedEvent( string error )
	{
		Debug.Log( "billingNotSupportedEvent: " + error );
	}
	
	
	void queryInventorySucceededEvent( List<GooglePurchase> purchases, List<GoogleSkuInfo> skus )
	{
		Debug.Log( string.Format( "queryInventorySucceededEvent. total purchases: {0}, total skus: {1}", purchases.Count, skus.Count ) );
		Prime31.Utils.logObject( purchases );
		Prime31.Utils.logObject( skus );
	}
	
	
	void queryInventoryFailedEvent( string error )
	{
		Debug.Log( "queryInventoryFailedEvent: " + error );
	}
	
	
	void purchaseCompleteAwaitingVerificationEvent( string purchaseData, string signature )
	{
		Debug.Log( "purchaseCompleteAwaitingVerificationEvent. purchaseData: " + purchaseData + ", signature: " + signature );
	}
	
	
	void purchaseSucceededEvent( GooglePurchase purchase )
	{
		PlayerPrefs.SetInt("purchased", 1);
		loginScript.currentState = Login.Phase.Menu;
		loginScript.menu.gameObject.SetActive(true);
		loginScript.waitingForPurchase = false;
		loginScript.HideBG();
		loginScript.loaderRef.color= new Color(1,1,1,0);
		Debug.Log("purchase succesfull");
		Debug.Log( "purchaseSucceededEvent: " + purchase );
	}
	
	
	void purchaseFailedEvent( string error )
	{
		if (error == "Unable to buy item (response: 7:Item Already Owned)") {
			PlayerPrefs.SetInt("purchased", 1);
			loginScript.currentState = Login.Phase.Menu;
			loginScript.menu.gameObject.SetActive(true);
			loginScript.HideBG();
			loginScript.loaderRef.color= new Color(1,1,1,0);
		}
		loginScript.waitingForPurchase = false;

		Debug.Log( "purchaseFailedEvent: " + error );
	}
	
	#endif
}