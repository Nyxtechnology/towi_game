﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Prime31;

public class InAppPurchase : MonoBehaviour
{
#if UNITY_IPHONE
	private List<StoreKitProduct> _products;
	public bool canMakePayments = false;
	public bool productDataReceived = false;

#endif

	bool handHeld;
	float scaleSize;
	bool wifi;


	// Use this for initialization
	void Start ()
	{
		
		if(PlayerPrefs.GetInt("purchased") != 1)
		{
			PlayerPrefs.SetInt("purchased", 0);
		}


		scaleSize = (float)Screen.height / (float)768;
		if(SystemInfo.deviceType == DeviceType.Handheld)
		{
			handHeld = true;
		}

		if(handHeld)
		{
#if UNITY_ANDROID


			var key = "";
			key = "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAjlBGKnUg07hZpZL9n0yA8N2IE94Typxo/XlPoeuUUsX1ew4t5HpL+dbggv784jM2rAQgIUu86nx389x9tJ0svchtoeibUBaU8R6g0t6fgbfwgK6CeAN7Yq+Hln4ZhT2jN2l1P0bZaEunXK4Bhpy48q/zVj4J6nkPSv5cEhXuu8hrk8aClKqwjVsObeYxVdXan4UTELmRB8+6W6QkDwm2R8w7I00auAzOr8ZUxrUTESQpfhCZBFtuQpnRzyyaxXcsWUL6+rBzYdoamXCm10N9w0/Exk8X5Qjy0AIFVb+VZqui3m33Ra/V4ZXCU4C3qqmdrBq5upe1Ok/ZMNs0lnVkbQIDAQAB";
			IAP.init(key);


			var skus = new string[] {"one_time_purchase.001"};
			Debug.Log(skus);
			GoogleIAB.queryInventory( skus );


#endif

#if UNITY_IPHONE

			StoreKitManager.productListReceivedEvent += allProducts =>
			{
				Debug.Log( "received total products: " + allProducts.Count );
				_products = allProducts;
			};
			

#endif
		}

	}
	void Update()
	{
#if UNITY_IPHONE

		if(!productDataReceived)
		{
			productDataReceived = true;
			StoreKitBinding.requestProductData(new string[]{"one_time_purchase.001"});
		}

	

#endif		
	}
	public void Purchase()
	{
		#if UNITY_ANDROID
		GoogleIAB.purchaseProduct("one_time_purchase.001");
		#endif
		
		#if UNITY_IPHONE
		StoreKitBinding.purchaseProduct( "one_time_purchase.001", 1 );
		#endif
	}
}
