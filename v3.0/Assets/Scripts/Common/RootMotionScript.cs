﻿using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]

public class RootMotionScript : MonoBehaviour {

	float jumpWait=0.3f;
	bool doJump=false;
	bool jumped=false;
	private AnimatorStateInfo currentBaseState;
	static int locoState = Animator.StringToHash("Base Layer.Locomotion");
	static int backState = Animator.StringToHash("Base Layer.WalkBack");
	static int jumpState = Animator.StringToHash("Base Layer.Jump");

	void OnAnimatorMove()
	{
		Animator animator = GetComponent<Animator>(); 
		
		if (animator)
		{
			currentBaseState = animator.GetCurrentAnimatorStateInfo(0);
			Vector3 newPosition = transform.position;
			//newPosition.x += animator.GetFloat("Speed") *100* Time.deltaTime;
			//newPosition.z += animator.GetFloat("Direction") *100* Time.deltaTime;                                 
			if (currentBaseState.nameHash == locoState||currentBaseState.nameHash==jumpState){
				transform.Translate(Vector3.forward*animator.GetFloat("Speed") *7* Time.deltaTime);
				transform.Rotate(Vector3.up * Time.deltaTime *100* animator.GetFloat("Direction")*animator.GetFloat("Speed"));
			}else if(currentBaseState.nameHash == backState){
				transform.Translate(Vector3.forward*animator.GetFloat("Speed") *4* Time.deltaTime);
				transform.Rotate(Vector3.up * Time.deltaTime *100* animator.GetFloat("Direction")*((animator.GetFloat("Speed")<0)?-1:1)*animator.GetFloat("Speed"));
			}
			if(animator.GetBool("Jump")){
				if(!jumped)
				{
					jumped=true;
					doJump=true;
				}
			}else{
				jumped=false;
			}
			if(animator.GetBool("Turn")){
				//transform.Translate(Vector3.forward*1* Time.deltaTime);
				transform.Rotate(Vector3.up * Time.deltaTime *100*animator.GetFloat("Direction"));
			}
			//transform.position = newPosition;
		}
	}
	void Update(){
		if (doJump) {
			jumpWait-=Time.deltaTime;
			if(jumpWait<=0){
				transform.GetComponent<Rigidbody>().AddForce(Vector3.up*7000);
				doJump=false;
				jumpWait=0.3f;
			}
		}
	}

}