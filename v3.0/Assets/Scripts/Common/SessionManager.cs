﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Runtime.Serialization.Formatters.Binary;
using System.IO;
using Boomlagoon.JSON;
using System.Text;
using System.Xml.Serialization;


public class SessionManager : MonoBehaviour
{

    List<User> users;
    public User activeUser;
    public Kid activeKid;
    public bool main = false;
    string syncProfileURL = "http://www.towi.com.mx/api/sync_profiles.php";
    public List<Kid> temporalKids;

    void Awake()
    {
        //if(main)
        //	PlayerPrefs.DeleteAll ();
        //PlayerPrefs.DeleteAll ();
        activeUser = null;
        activeKid = null;
        //PlayerPrefs.SetString ("sessions", "");
        users = new List<User>();
        temporalKids = new List<Kid>();
        LoadSession();
        if (users.Count == 0)
        {
            AddUser("_local", "", "_local", null);
            if (activeUser != null)
                activeUser.kids.Add(new Kid(0, "local_kid", "_local"));
            activeUser = null;
            SaveSession();
        }
        if (main)
        {
            /*if(PlayerPrefs.GetInt("SubscriptionTrial",0)==0)
            {
                PlayerPrefs.SetString("activeUser","");	
            }
            else
            {
                LoadActiveUser();
            }
            PlayerPrefs.SetInt("activeKid",-1);*/
        }
        else
        {
            string userS = PlayerPrefs.GetString("activeUser", "");
            if (userS != "")
            {
                activeUser = GetUser(userS);
                int kidS = PlayerPrefs.GetInt("activeKid", -1);
                if (kidS != -1)
                {
                    activeKid = GetKid(kidS);
                }
            }
        }
    }

    public int GetNumberOfUsers()
    {
        return users.Count;
    }

    public User GetUser(string key)
    {
        for (int i = 0; i < users.Count; i++)
        {
            for (int c = 0; c < users[i].kids.Count; c++)
            {
                if (users[i].kids[c].userkey == key||(key=="_local"&&users[i].userkey==key))
                {
                    return users[i];
                }
            }
        }
        return null;
    }

    Kid GetKid(int id)
    {
        for (int i = 0; i < activeUser.kids.Count; i++)
        {
            if (activeUser.kids[i].id == id)
            {
                return activeUser.kids[i];
            }
        }
        return null;
    }

    public bool FindUser(string username)
    {
        for (int i = 0; i < users.Count; i++)
        {
            if (users[i].username == username)
            {
                return true;
            }
        }
        return false;
    }

    public int TryLogin(string username, string psswd)
    {
        for (int i = 0; i < users.Count; i++)
        {
            if (users[i].username == username && users[i].psswdHash == psswd)
            {
                if (DateTime.Now <= users[i].suscriptionDate)
                {
                    activeUser = users[i];
                    activeKid = activeUser.kids[0];
                    PlayerPrefs.SetString("activeUser", activeKid.userkey);
                    PlayerPrefs.SetInt("activeKid", 1);
                    SyncProfiles(activeKid.userkey);
                    return 1;
                }
                else
                {
                    return 2;
                }
            }
        }
        return -1;
    }

    public void Logout()
    {
        PlayerPrefs.SetString("activeUser", "");
        PlayerPrefs.SetInt("activeKid", -1);
        activeKid = null;
        activeUser = null;
    }

    public void LoadLocal()
    {
        activeUser = users[0];
        activeKid = activeUser.kids[0];
        PlayerPrefs.SetString("activeUser", "_local");
        PlayerPrefs.SetInt("activeKid", activeKid.id);
    }

    public bool LoadActiveUser()
    {
        string userS = PlayerPrefs.GetString("activeUser", "");
        if (userS != "")
        {
            activeUser = GetUser(userS);
            return true;
        }
        return false;
    }

    public void LoadUser(string username, string psswd, string key, string[] kids)
    {
        bool missing = true;
        if (users.Count > 0)
        {
            for (int i = 0; i < users.Count; i++)
            {
                if (users[i].userkey == key)
                {
                    missing = false;
                    users[i].username = username;
                    users[i].psswdHash = psswd;
                    SaveSession();
                    activeUser = users[i];
                    if (activeUser.kids.Count > 0)
                    {
                        activeKid = activeUser.kids[0];
                        PlayerPrefs.SetInt("activeKid", activeKid.id);
                    }
                    else
                    {
                        activeKid = null;
                        PlayerPrefs.SetInt("activeKid", -1);
                    }
                    //PlayerPrefs.SetString("activeUser", activeKid.userkey);
                    //SyncProfiles();
                    break;
                }
            }
        }
        if (missing)
        {
            users.Add(new User(key, username, psswd));

            activeUser = users[users.Count - 1];
            if (activeUser.kids.Count > 0)
            {
                activeKid = activeUser.kids[0];
                PlayerPrefs.SetInt("activeKid", activeKid.id);
            }
            else
            {
                activeKid = null;
                PlayerPrefs.SetInt("activeKid", -1);
            }
            if (activeKid != null)
                PlayerPrefs.SetString("activeUser", activeKid.userkey);
            SaveSession();
        }
    }
    public void AddUser(string username, string psswd, string key, string[] kids)
    {
        if (users.Count > 0)
        {
            for (int i = 0; i < users.Count; i++)
            {
                if (users[i].username == username)
                {
                    return;
                }
            }
        }
        users.Add(new User(key, username, psswd));
        activeUser = users[users.Count - 1];
        SaveSession();
    }
    public void AddKid(int kidID, string name, string key)
    {
        activeUser.kids.Add(new Kid(kidID, name, key));
        SaveSession();
    }
    public void AddKids(JSONArray kids)
    {
        temporalKids.Clear();
        for (int i = 0; i < kids.Length; i++)
        {
            JSONObject kidObj = kids[i].Obj;
            bool missingUser = true;
            bool missing = true;
            for (int u = 0; u < users.Count; u++)
            {
                if (users[u].userkey == kidObj.GetValue("key").Str)
                {
                    missingUser = false;
                    for (int k = 0; k < users[u].kids.Count; k++)
                    {
                        if (users[u].kids[k].id == int.Parse(kidObj.GetValue("cid").Str))
                        {
                            if (!bool.Parse(kidObj.GetValue("active").Str) && !bool.Parse(kidObj.GetValue("trial").Str))
                            {
                                users[u].kids.RemoveAt(k);
                            }
                            else
                            {
                                users[u].kids[k].name = kidObj.GetValue("name").Str + " " + kidObj.GetValue("lastname").Str;
                                users[u].kids[k].userkey = kidObj.GetValue("key").Str;
                                temporalKids.Add(users[u].kids[k]);
                            }
                            missing = false;
                            break;
                        }
                    }
                    if (missing)
                    {
                        //byte[] uni = Encoding.Unicode.GetBytes(kidObj.GetValue("name").Str+" "+kidObj.GetValue("lastname").Str);
                        //string ascii = Encoding.ASCII.GetString(uni);
                        if (bool.Parse(kidObj.GetValue("active").Str) || bool.Parse(kidObj.GetValue("trial").Str))
                        {
                            users[u].kids.Add(new Kid(int.Parse(kidObj.GetValue("cid").Str), kidObj.GetValue("name").Str + " " + kidObj.GetValue("lastname").Str, kidObj.GetValue("key").Str));
                            temporalKids.Add(users[u].kids[users[u].kids.Count - 1]);
                        }
                    }
                }
            }
            if (missingUser)
            {
                users.Add(new User(kidObj.GetValue("key").Str, "", ""));
                if (bool.Parse(kidObj.GetValue("active").Str) || bool.Parse(kidObj.GetValue("trial").Str))
                {
                    users[users.Count - 1].kids.Add(new Kid(int.Parse(kidObj.GetValue("cid").Str), kidObj.GetValue("name").Str + " " + kidObj.GetValue("lastname").Str, kidObj.GetValue("key").Str));
                    temporalKids.Add(users[users.Count - 1].kids[users[users.Count - 1].kids.Count - 1]);
                }
            }
        }
        /*for (int k=0; k< activeUser.kids.Count; k++) {
            bool exists=false;
            for(int i=0;i<kids.Length;i++)
            {
                JSONObject kidObj=kids[i].Obj;
                if(activeUser.kids[k].id==int.Parse(kidObj.GetValue("cid").Str))
                {
                    exists=true;
                    break;
                }
            }
            if(!exists)
            {
                activeUser.kids.RemoveAt(k--);
            }
        }*/
        //activeKid = activeUser.kids [0];
        //PlayerPrefs.SetInt("activeKid",activeKid.id);
        SaveSession();
    }

    public void SetKid(string parentkey, int id)
    {
        for (int u = 0; u < users.Count; u++)
        {
            if (users[u].userkey == parentkey)
            {
                for (int i = 0; i < users[u].kids.Count; i++)
                {
                    if (users[u].kids[i].id == id)
                    {
                        activeKid = users[u].kids[i];
                    }
                }
            }
        }
        PlayerPrefs.SetInt("activeKid", id);
        PlayerPrefs.SetString("activeUser", activeKid.userkey);
        SaveSession();
    }
    public void SaveSession()
    {
        Environment.SetEnvironmentVariable("MONO_REFLECTION_SERIALIZER", "yes");

        /*
        XmlSerializer serializer = new XmlSerializer(typeof(List<User>));
        MemoryStream stream = new MemoryStream();
        serializer.Serialize(stream, users);
        PlayerPrefs.SetString("sessions", Convert.ToBase64String(stream.GetBuffer()));
        */
        BinaryFormatter b = new BinaryFormatter();
        //Create an in memory stream
        MemoryStream m = new MemoryStream();
        //Save the scores
        b.Serialize(m, users);
        //Add it to player prefs
        PlayerPrefs.SetString("sessions", Convert.ToBase64String(m.GetBuffer()));
    }

    void LoadSession()
    {
        //Get the data
        string data = PlayerPrefs.GetString("sessions");
        //If not blank then load it
        if (!string.IsNullOrEmpty(data))
        {
            Environment.SetEnvironmentVariable("MONO_REFLECTION_SERIALIZER", "yes");
            /*
            XmlSerializer serializer = new XmlSerializer(typeof(List<User>));
            MemoryStream stream = new MemoryStream(Convert.FromBase64String(data));
            users = (List<User>)serializer.Deserialize(stream);
            */


            //Binary formatter for loading back
            BinaryFormatter b = new BinaryFormatter();
            //Create a memory stream with the data
            MemoryStream m = new MemoryStream(Convert.FromBase64String(data));
            //Load back the scoress
            users = (List<User>)b.Deserialize(m);
        }
    }

    public void SyncProfiles(string key)
    {
        StartCoroutine(PostSyncProfiles(key));
    }

    IEnumerator PostSyncProfiles(string key)
    {
        string post_url = syncProfileURL;

        WWWForm form = new WWWForm();
        form.AddField("userKey", key);

        WWW hs_post = new WWW(post_url, form);
        yield return hs_post;
        if (hs_post.error == null)
        {
            JSONObject jsonObject = JSONObject.Parse(hs_post.text);
            if (jsonObject.GetValue("code").Str == "200")
            {
                JSONArray kids = jsonObject.GetValue("profiles").Array;
                for (int i = 0; i < kids.Length; i++)
                {
                    JSONObject kidObj = kids[i].Obj;
                    JSONObject activeMissions = kidObj.GetValue("activeMissions").Obj;
                    JSONObject missionList = kidObj.GetValue("missionList").Obj;
                    for (int u = 0; u < users.Count; u++)
                    {
                        if (users[u].userkey == key)
                        {
                            for (int k = 0; k < users[u].kids.Count; k++)
                            {
                                if (users[u].kids[k].id == int.Parse(kidObj.GetValue("cid").Str) && users[u].kids[k].syncProfile)
                                {
                                    users[u].kids[k].kiwis = int.Parse(kidObj.GetValue("kiwis").Str);
                                    users[u].kids[k].avatar = kidObj.GetValue("avatar").Str;
                                    users[u].kids[k].avatarClothes = kidObj.GetValue("avatarClothes").Str;
                                    users[u].kids[k].ownedItems = kidObj.GetValue("ownedItems").Str;
                                    users[u].kids[k].activeMissions = activeMissions.ToString();
                                    users[u].kids[k].missionList = missionList != null ? missionList.ToString() : "";
                                    users[u].kids[k].activeDay = int.Parse(kidObj.GetValue("activeDay").Str);
                                    users[u].kids[k].rioTutorial = int.Parse(kidObj.GetValue("rioTutorial").Str);
                                    users[u].kids[k].tesoroTutorial = int.Parse(kidObj.GetValue("tesoroTutorial").Str);
                                    users[u].kids[k].arbolMusicalTutorial = int.Parse(kidObj.GetValue("arbolMusicalTutorial").Str);
                                    users[u].kids[k].ageSet = true;
                                    break;
                                }
                            }
                        }
                    }
                    SaveSession();
                }
            }
        }
        else
        {
            Debug.Log(hs_post.error);
        }
    }

    //
    [System.Serializable]
    public class User
    {
        public string userkey;
        public string username;
        public string psswdHash;
        public List<Kid> kids;
        public string language;
        public bool trialAccount;
        public DateTime suscriptionDate;
        public User(string key, string user, string psswd)
        {
            language = "es";
            userkey = key;
            username = user;
            psswdHash = psswd;
            kids = new List<Kid>();
            trialAccount = true;
            suscriptionDate = DateTime.Now;
            suscriptionDate.AddDays(7);
        }
    }
    [System.Serializable]
    public class Kid
    {
        public string userkey;
        public int id;
        public string name;
        public int kiwis;
        public int dontSyncArbolMusical;
        public int dontSyncRio;
        public int dontSyncArenaMagica;
        public int dontSyncDondeQuedoLaBolita;
        public int dontSyncSombras;
        public int dontSyncTesoro;

        public string avatar;
        public string avatarClothes;

        public string offlineData;
        public string activeMissions;
        public string missionList;
        public string ownedItems;
        public int activeDay;
        public bool ageSet;

        public int arbolMusicalLevel;
        public int arbolMusicalSublevel;
        public int rioLevel;
        public int rioSublevel;
        public int arenaMagicaLevel;
        public int arenaMagicaSublevel;
        public int monkeyLevel;
        public int monkeySublevel;
        public int sombrasLevel;
        public int sombrasSublevel;
        public int tesoroLevel;
        public int tesoroSublevel;

        public int playedArbolMusical;
        public int blockedArbolMusical;
        public int playedRio;
        public int blockedRio;
        public int playedArenaMagica;
        public int blockedArenaMagica;
        public int playedSombras;
        public int blockedSombras;
        public int playedDondeQuedoLaBolita;
        public int blockedDondeQuedoLaBolita;
        public int playedTesoro;
        public int blockedTesoro;

        public int rioTutorial;
        public int tesoroTutorial;
        public int arbolMusicalTutorial;

        public string xmlArbolMusical;
        public string xmlRio;
        public string xmlArenaMagica;
        public string xmlSombras;
        public string xmlDondeQuedoLaBolita;
        public string xmlTesoro;

        public int extraField1;
        public int extraField2;

        public bool syncProfile = true;

        public Kid(int id, string name, string key)
        {
            this.id = id;
            this.name = name;
            userkey = key;
            kiwis = 0;
            dontSyncArbolMusical = 0;
            dontSyncRio = 0;
            dontSyncArenaMagica = 0;
            dontSyncDondeQuedoLaBolita = 0;
            dontSyncSombras = 0;
            dontSyncTesoro = 0;

            avatar = "";
            avatarClothes = "";

            offlineData = "";
            activeMissions = "";
            missionList = "";
            ownedItems = "";
            activeDay = -1;
            ageSet = false;

            arbolMusicalLevel = 0;
            arbolMusicalSublevel = 0;
            arbolMusicalTutorial = 1;
            rioLevel = 0;
            rioSublevel = 0;
            rioTutorial = 0xfffffff;
            arenaMagicaLevel = 0;
            arenaMagicaSublevel = 0;
            monkeyLevel = 0;
            monkeySublevel = 0;
            sombrasLevel = 0;
            sombrasSublevel = 0;
            tesoroLevel = 0;
            tesoroSublevel = 0;
            tesoroTutorial = 0;

            playedArbolMusical = 0;
            blockedArbolMusical = 0;
            playedRio = 0;
            blockedRio = 0;
            playedArenaMagica = 0;
            blockedArenaMagica = 0;
            playedSombras = 0;
            blockedSombras = 0;
            playedDondeQuedoLaBolita = 0;
            blockedDondeQuedoLaBolita = 0;
            playedTesoro = 0;
            blockedTesoro = 0;

            xmlArbolMusical = "";
            xmlRio = "";
            xmlArenaMagica = "";
            xmlSombras = "";
            xmlDondeQuedoLaBolita = "";
            xmlTesoro = "";

            syncProfile = true;
        }
    }
}
