using UnityEngine;
using System.Collections;

public class ThirdPersonCamera : MonoBehaviour
{
	public float smooth = 3f;		// a public variable to adjust smoothing of camera motion
	public Transform standardPos;			// the usual position for the camera, specified by a transform in the game
	Transform lookAtPos;			// the position to move the camera to when using head look
	public GameObject cameraRef;
	void Start()
	{
		// initialising references
		//standardPos = cameraRef.transform;
	}
	
	void Update ()
	{
		//if (standardPos == null) {
		//		standardPos = GameObject.Find ("Avatar").transform;
		//}

		// return the camera to standard position and direction
		transform.position = standardPos.position;	
		transform.forward = standardPos.forward;		

		//transform.position = Vector3.Lerp(transform.position, standardPos.position, /*Time.deltaTime * smooth*/0.5f);	
		//transform.forward = Vector3.Lerp(transform.forward, standardPos.forward, /*Time.deltaTime * smooth*/0.5f);		
	}
}
