#if UNITY_ANDROID
using UnityEngine;
using System.Collections;

public class DownloadObbExample : MonoBehaviour {
	private string expPath; 
	private bool downloadStarted;

	void Start()
	{
		expPath = GooglePlayDownloader.GetExpansionFilePath();
		
		if (expPath == null)
		{
			//Error
			System.Console.WriteLine("\nOBB DOWNLOAD: External Path not found\n");
			return;
		}
		else
		{
			string mainPath = GooglePlayDownloader.GetMainOBBPath(expPath);
			string patchPath = GooglePlayDownloader.GetPatchOBBPath(expPath);
			
			System.Console.WriteLine("\nOBB DOWNLOAD: External Path: " + expPath +"\n");
			System.Console.WriteLine("\nOBB DOWNLOAD: Main Path: " + mainPath +"\n");
			System.Console.WriteLine("\nOBB DOWNLOAD: Patch Path: " + mainPath +"\n");
			
			if (mainPath == null)
			{
				System.Console.WriteLine("\nOBB DOWNLOAD: Main was null and OBB will be fetched now\n");
				
				GooglePlayDownloader.FetchOBB();
				
				System.Console.WriteLine("\nOBB DOWNLOAD: OBB has been fetched\n");
				
				StartCoroutine(loadLevel());
			}
			else
				Application.LoadLevel("Login");
		}
	}
	
	protected IEnumerator loadLevel()
	{
		string mainPath;
		
		do
		{
			System.Console.WriteLine("\nOBB DOWNLOAD: Trying to fetch main path\n");
			
			yield return new WaitForSeconds(0.5f);
			mainPath = GooglePlayDownloader.GetMainOBBPath(expPath);
			
			System.Console.WriteLine("\nOBB DOWNLOAD: Main path = "+ mainPath + "\n");
			
		}
		while( mainPath == null);
		
		if(downloadStarted == false )
		{
			System.Console.WriteLine("\nOBB DOWNLOAD: Load OBB from mainPath: "+ mainPath +"\n");
			
			downloadStarted = true;
			
			string uri = "file://" + mainPath;
			
			WWW www = WWW.LoadFromCacheOrDownload(uri , 0);
			
			// Wait for download to complete
			yield return www;
			
			System.Console.WriteLine("\nOBB DOWNLOAD: OBB loaded. Procede to next level...\n");
			
			Application.LoadLevel("Login");
		}
	}
}
#endif